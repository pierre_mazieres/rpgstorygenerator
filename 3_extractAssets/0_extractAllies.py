#!/usr/bin/env python3
'''
list allies from an RPG story
 - input : 1 RPG story is 1 txt file
 - output : 1 liste of allies is 1 txt file
'''
from openai import OpenAI
import sys
import tiktoken
import os
import shutil
import time
import math
root=sys.path[0]+"/"
output=root+"output/"
model="gpt-4"
encoding = tiktoken.encoding_for_model(model)
client = OpenAI()
resultTokenNumber=0
storyLanguage="French"
systemContent="You are an RPG story generator. You are given a summary of a chapter of a novel as input. Your output must be a scenario for an RPG game corresponding to this chapter. Replace each occurrence of the protagonists ("+protagonists+") with 'the protagonists'. This story should be in "+storyLanguage+" language."
systemTokens = len(encoding.encode(systemContent))
responseTokensRetention=.99 # keep some spare tokens / space
# clean
if (os.path.exists(output)):
    shutil.rmtree(output)
os.makedirs(output)
# each chapter / file
resultTokenNumber=0
for inputFolder, _, storyFilesNames in os.walk(root+"/input"):
    storyFilesNames.sort()
    for storyFilesName in storyFilesNames:
        # read all story
        storyFilePath = str(os.path.join(inputFolder,storyFilesName))
        storyFiles = open(storyFilePath, "r")
        storyContent = storyFiles.read()
        storyFiles.close()
        if len(storyContent)>0:
            # story file
            storyFilePath = os.path.join(output,storyFilesName)
            storyFile = open(storyFilePath,"w")
            # compute response tokens
            userTokens = len(encoding.encode(storyContent))
            responseTokens=math.floor((8192-systemTokens-userTokens)*responseTokensRetention)
            # check paragraph can be resumed
            if responseTokens<=0:
                raise RuntimeError("ERROR : response space is laking tokens : " + str(abs(responseTokens)))
            # story
            choice = client.chat.completions.create(
                model=model,
                messages=[
                    {"role": "system", "content": systemContent},
                    {"role": "user", "content": storyContent}
                ],
                max_tokens=responseTokens,
            ).choices[0]
            # check finish reason
            if choice.finish_reason!="stop":
                raise RuntimeError("ERROR : story did not finish well : " + choice.finish_reason)
            # write story
            print(choice.message.content,file=storyFile)
            # wait for new tokens
            storyTokenNumber = len(encoding.encode(choice.message.content))
            resultTokenNumber=resultTokenNumber+storyTokenNumber
            if(resultTokenNumber>=10000):
                time.sleep(60)
                resultTokenNumber=0
            # close story
            storyFile.close()
# clean
client.close()
