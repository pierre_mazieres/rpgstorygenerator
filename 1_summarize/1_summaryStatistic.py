#!/usr/bin/env python3
'''
statistics to prepare chapter handling by LLM
 - input : 1 summary is 1 txt file
 - output : 1 static file for summarys & chapters
'''
import tiktoken
import os
import statistics
import sys
root=sys.path[0]+"/"
# set GPT version
encoding = tiktoken.encoding_for_model("gpt-4")
# read all texte files
summaryTokenCounts = []
biggestSummary=(0,"")
# each summary / file
for rootFolder, _, summaryNames in os.walk(root+"../1_summarize/output"):
    summaryNames.sort()
    for summaryName in summaryNames:
        filePath = str(os.path.join(rootFolder,summaryName))
        # read summary file
        summaryFile= open(filePath, "r")
        summary = summaryFile.read()
        summaryFile.close()
        # count chapter token
        summaryTokenCount = len(encoding.encode(summary))
        summaryTokenCounts.append(summaryTokenCount)
        # bigger summary
        if summaryTokenCount>biggestSummary[0]:
            biggestSummary=(summaryTokenCount,summaryName)
# statistics
summaryMin = min(summaryTokenCounts)
summaryMedian = round(statistics.median(summaryTokenCounts))
summaryMean = round(statistics.mean(summaryTokenCounts))
summaryStdDev = round(statistics.pstdev(summaryTokenCounts))
statisticFile = open(root+"../1_summarize/statistic.txt", "w") # . ; ../2_toRpgStory
print("min : "+str(summaryMin), file=statisticFile)
print("max : "+str(biggestSummary), file=statisticFile)
print("median : "+str(summaryMedian), file=statisticFile)
print("mean : "+str(summaryMean), file=statisticFile)
print("std dev : "+str(summaryStdDev), file=statisticFile)
statisticFile.close()
