#!/usr/bin/env python3
'''
sumarrize a novel chapter
 - input :
   1 chapter is 1 folder
   1 set of paragraphs is 1 file
 - output : 1 summary is 1 txt file
'''
from openai import OpenAI
import sys
import tiktoken
import os
import shutil
import time
import math
root=sys.path[0]+"/"
output=root+"output/"
model="gpt-4"
encoding = tiktoken.encoding_for_model(model)
client = OpenAI()
resultTokenNumber=0
summaryLanguage="French"
systemContent="You are a text summarizer. You are given an input text. You must provide a summary of this text as your response. This summary should be in "+summaryLanguage+" language."
systemTokens = len(encoding.encode(systemContent))
responseTokensRetention=.99 # keep some spare tokens / space
# clean
if os.path.exists(output):
    shutil.rmtree(output)
os.makedirs(output)
# each chapter / folder
for txtRootFolder, txtSubFolders, _ in os.walk(root+"input/"):
    txtSubFolders.sort()
    for txtSubFolder in txtSubFolders:
        # initialize summary
        summaryPath = output+txtSubFolder+".txt"
        summaryFile = open(summaryPath, "w")
        # each paragraphs set / file
        txtSubFolderPath = txtRootFolder+txtSubFolder
        for _, _, txtFilesNames in os.walk(txtSubFolderPath):
            txtFilesNames.sort()
            for txtFilesName in txtFilesNames:
                # read all txt
                txtFilePath = str(os.path.join(txtSubFolderPath,txtFilesName))
                txtFiles = open(txtFilePath, "r")
                txtContent = txtFiles.read()
                txtFiles.close()
                if len(txtContent)>0:
                    # compute response tokens
                    userTokens = len(encoding.encode(txtContent))
                    responseTokens=math.floor((8192-systemTokens-userTokens)*responseTokensRetention)
                    # check paragraph can be resumed
                    if responseTokens<=0:
                        raise RuntimeError("ERROR : response space is laking tokens : " + str(abs(responseTokens)))
                    # summarize
                    choice = client.chat.completions.create(
                        model=model,
                        messages=[
                            {"role": "system", "content": systemContent},
                            {"role": "user", "content": txtContent}
                        ],
                        max_tokens=responseTokens,
                    ).choices[0]
                    # check finish reason
                    if choice.finish_reason!="stop":
                        raise RuntimeError("ERROR : summary did not finish well : " + choice.finish_reason)
                    # write summary
                    print(choice.message.content,file=summaryFile)
                    # wait for new tokens
                    summaryTokenNumber = len(encoding.encode(choice.message.content))
                    resultTokenNumber=resultTokenNumber+summaryTokenNumber
                    if(resultTokenNumber>=10000):
                        time.sleep(60)
                        resultTokenNumber=0
            # close summary
            summaryFile.close()
# clean
client.close()
