#!/usr/bin/env python3
'''
statistics to prepare chapter handling by LLM
 - input :
   1 chapter is 1 folder
   1 paragraph is 1 file
 - output : 1 static file for paragraphs & chapters
'''
import tiktoken
import os
import statistics
import sys
root=sys.path[0]+"/"
# set GPT version
encoding = tiktoken.encoding_for_model("gpt-4")
# read all texte files
paragraphTokenCounts = []
biggestParagraph=(0,"")
chapterTokenCounts = []
biggestChapter=(0,"")
# each chapter / folder
for rootFolder, chapterNames, _ in os.walk(root+"output/group"):
    chapterNames.sort()
    for chapterName in chapterNames:
        chapterFolder=os.path.join(rootFolder,chapterName)
        chapterTokenCount = 0
        # each paragraph / file
        for _, _, paragraphNames in os.walk(chapterFolder):
            paragraphNames.sort()
            for paragraphName in paragraphNames:
                filePath = str(os.path.join(chapterFolder,paragraphName))
                # read paragraph file
                paragraphFile= open(filePath, "r")
                paragraph = paragraphFile.read()
                paragraphFile.close()
                # count chapter token
                paragraphTokenCount = len(encoding.encode(paragraph))
                paragraphTokenCounts.append(paragraphTokenCount)
                # bigger paragraph
                if paragraphTokenCount>biggestParagraph[0]:
                    biggestParagraph=(paragraphTokenCount,chapterName)
                # chapter token
                chapterTokenCount = chapterTokenCount+paragraphTokenCount
        # count chapter token
        chapterTokenCounts.append(chapterTokenCount)
        # bigger chapter
        if chapterTokenCount>biggestChapter[0]:
            biggestChapter=(chapterTokenCount,chapterName)
# statistics
# ... paragraph
paragraphMin = min(paragraphTokenCounts)
paragraphMedian = round(statistics.median(paragraphTokenCounts))
paragraphMean = round(statistics.mean(paragraphTokenCounts))
paragraphStdDev = round(statistics.pstdev(paragraphTokenCounts))
# ... chapter
chapterMin = min(chapterTokenCounts)
chapterMedian = round(statistics.median(chapterTokenCounts))
chapterMean = round(statistics.mean(chapterTokenCounts))
chapterStdDev = round(statistics.pstdev(chapterTokenCounts))
statisticFile = open(root+"statisticGroup.txt", "w") # split / group
print("PARAGRAPH", file=statisticFile)
print("min : "+str(paragraphMin), file=statisticFile)
print("max (chapter name) : "+str(biggestParagraph), file=statisticFile)
print("median : "+str(paragraphMedian), file=statisticFile)
print("mean : "+str(paragraphMean), file=statisticFile)
print("std dev : "+str(paragraphStdDev), file=statisticFile)
print("CHAPTER", file=statisticFile)
print("min : "+str(chapterMin), file=statisticFile)
print("max : "+str(biggestChapter), file=statisticFile)
print("median : "+str(chapterMedian), file=statisticFile)
print("mean : "+str(chapterMean), file=statisticFile)
print("std dev : "+str(chapterStdDev), file=statisticFile)
statisticFile.close()
