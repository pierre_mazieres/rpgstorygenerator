#!/usr/bin/env python3
'''
convert a chapter from xHTML (ePUB) to txt
 - input : 1 chapter is 1 html file
 - output :
   1 chapter is 1 folder
   1 paragraph is 1 file
'''
from html.parser import HTMLParser
import sys
import shutil
import os
import re
import math
root=sys.path[0]+"/"
output=root+"output/split/"
# HTML parser
class MyHTMLParser(HTMLParser):
    def feed(self, data):
        self.isParagraph=False
        self.paragraphs=[]
        self.paragraph=""
        super().feed(data)
    def handle_starttag(self, tag, _):
        if tag=="p":
            self.isParagraph=True
    def handle_endtag(self, tag):
        if tag=="p":
            stripParagraph=self.paragraph.strip()
            # validate paragraph
            if len(stripParagraph)>0:
                # store paragraph
                self.paragraphs.append(stripParagraph)
            # next paragraph
            self.paragraph = ""
            self.isParagraph=False
    def handle_data(self, data):
        if self.isParagraph:
            self.paragraph = self.paragraph + data + "\n"
parser = MyHTMLParser()
# clean
if os.path.exists(output):
    shutil.rmtree(output)
os.makedirs(output)
# each HTML
for htmlFolder, _, htmlFileNames in os.walk(root+"input"):
    htmlFileNames.sort()
    for htmlFileName in htmlFileNames:
        # read HTML
        htmlFilePath = str(os.path.join(htmlFolder,htmlFileName))
        htmlFile = open(htmlFilePath, "r")
        fileContent = htmlFile.read()
        htmlFile.close()
        # parse HTML
        parser.feed(fileContent)
        parser.close()
        # validate paragraphs
        if len(parser.paragraphs)>0:
            # create chapter folder
            shortName = htmlFileName[:-6] # .xHTML -> 6 chars
            chapterFolder = output + shortName + "/"
            if not os.path.exists(chapterFolder):
                os.makedirs(chapterFolder)
            # compute pagination
            paragraphFilePattern = chapterFolder + shortName
            paragraphNumber = len(parser.paragraphs)
            digitNumber = math.ceil(math.log10(paragraphNumber))
            numberFormat = "{:0"+str(digitNumber)+".0f}"
            paragraphIndex = 0
            for paragraph in parser.paragraphs:
                # write paragraph
                paragraphFileName = paragraphFilePattern+"_"+numberFormat.format(paragraphIndex)+".txt"
                paragraphFile = open(paragraphFileName, "w")
                paragraphFile.write(paragraph)
                paragraphFile.close()
                # next paragraph
                paragraphIndex = paragraphIndex+1