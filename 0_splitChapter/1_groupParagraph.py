#!/usr/bin/env python3
'''
some paragraphs are very small
we group them in order to improve summarization
 - input :
   1 chapter is 1 folder
   1 paragraph is 1 file
 - output :
   1 chapter is 1 folder
   1 set of paragraphs is 1 file
'''
import tiktoken
import os
import shutil
import sys
import math
root=sys.path[0]+"/"
output=root+"output/group/"
tokensRetentionRate = .75 # left space for summary response
maxTokens = math.floor(8192*tokensRetentionRate)
encoding = tiktoken.encoding_for_model("gpt-4")
# clean
if os.path.exists(output):
    shutil.rmtree(output)
os.makedirs(output)
# each chapter / folder
for splitFolder, chapterNames, _ in os.walk(root+"output/split"):
    chapterNames.sort()
    for chapterName in chapterNames:
        # folder
        splitChapterFolder=os.path.join(splitFolder,chapterName)
        groupChapterFolder=os.path.join(output,chapterName)
        os.makedirs(groupChapterFolder)
        groupParagraphFilePattern = os.path.join(groupChapterFolder,chapterName)
        # group
        groupParagraphs=[]
        # each split paragraph / file
        groupParagraph = ""
        tokenCounter = 0
        for _, _, splitParagraphNames in os.walk(splitChapterFolder):
            splitParagraphNames.sort()
            for splitParagraphName in splitParagraphNames:
                # read split paragraph
                splitFilePath = str(os.path.join(splitChapterFolder,splitParagraphName))
                splitParagraphFile= open(splitFilePath, "r")
                splitParagraph = splitParagraphFile.read()
                splitParagraphFile.close()
                splitParagraphToken = len(encoding.encode(splitParagraph))
                # group paragraph
                candidateParagraph = groupParagraph + "\n" + splitParagraph
                tokenCounter = tokenCounter + splitParagraphToken
                # enough paragraph
                if tokenCounter>maxTokens:
                    # add group
                    groupParagraphs.append(groupParagraph.strip())
                    # reset
                    groupParagraph = splitParagraph
                    tokenCounter = len(encoding.encode(groupParagraph))
                # more paragraph
                else:
                    groupParagraph = candidateParagraph
        # last group
        groupParagraphs.append(groupParagraph.strip())
        # compute pagination
        groupParagraphNumber = len(groupParagraphs)
        digitNumber = math.ceil(math.log10(groupParagraphNumber))
        numberFormat = "{:0"+str(digitNumber)+".0f}"
        groupParagraphIndex = 0
        for groupParagraph in groupParagraphs:
            # write group paragraph
            groupParagraphFileName = groupParagraphFilePattern+"_"+numberFormat.format(groupParagraphIndex)+".txt"
            groupParagraphFile = open(groupParagraphFileName, "w")
            groupParagraphFile.write(groupParagraph)
            groupParagraphFile.close()
            # next group paragraph
            groupParagraphIndex = groupParagraphIndex+1
        #
